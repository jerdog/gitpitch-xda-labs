XDA Labs: A Community-Centric App Market
Today?
- Apps almost always distributed on Google Play
- 30% tax for the privilege of the platform
- But now your business depends entirely on Google
The Problem?
- Google is a big company, and they aren't developer-focused
- Any issues are handled by "bots"
- Bot loops aren't unheard of - good luck finding a real human
- Not brilliant when your app is offline, or under threat of removal!
The Solution?
- We need a platform for app distribution that's not hostile to developers
- Where you can talk to a real person, and you're not just another number
- But also one that's willing to innovate and make new things
Examples?
- Gift a paid app to someone else?
- Alternative payment methods?
- Try to improve the review/rating experience
 (add some more here)
Reviews?
- Users can rate your app, and leave comments
- Many users writing reviews are clueless
- Independent developers lose out from this
  - "Make this support video and I'll give it 5*"
- Almost a tool for "ransoming" devs to work for them
- This is fair enough if you're a big company maybe
- Less so when you're an independent developer
- What if your app is also free? It's demoralising
- We need to try to solve the review problem
- Short reviews, even shorter responses. This makes it hard for developers
- Reviews need to "fade" over time, especially when referring to old versions
  - but don't punish developers for updating (see iOS)
- Some reviews are frankly more insightful than others
  - We need to prioritise well-informed, helpful reviews (be they positive or negative)
A Solution?
- Lofty goals!
- But we may as well make a start!
Presenting...
- XDA Labs
- By the team at XDA
- Building a new platform for distribution of apps
- Community-first, by devs for the community
- Devs deserve another choice
Why Labs?
- Bootstrapping on XDA's established reputation in the community
- More technical than average audience
- Good proving ground for your app
- Don't worry about Google killing your developer account!
Won't you just become Google?
- 0% fee on sales/transactions - you keep it all
- Do them how you like
- Labs is our way to give something back to developers
- Today we support Paypal and Bitcoin - we'd love to support more methods in future [Note to jer: we could do stripe/square and charge a small fee on that for some $$$]
What's Different?
- Much simpler rules. Don't mislead people. Don't break the law.
- We keep it simple!
- You can talk to us easily. In fact, we're a community, so we're built around talking!
- No "bot" support, and we won't be having any!
- We understand life as an indie dev - we don't insist on SLAs on email support
- We won't make you put your physical address on your developer profile and say "no PO Boxes"
- We don't update our rules every 5 minutes for every single edge case. It should be the closest we can get to "don't be an ass".
- We know about it! We had people distributing software/games and developing for 14 years. Google and Apple were miles off in 2002!
- We speak developer, and we've hired on community devs to build Labs, so it meets what devs actually want!
Why Labs? Why not self-distribute?
- Making an updater is a bit annoying. There's one built in! Don't re-invent the wheel
- You gain exposure to a (growing) market.
- Labs is focused on showcasing indie and homegrown talent - stand out!
- 
Where Next?
- We're always working to improve Labs. Let us know any issues!
- We've built a full replacement for Google Cloud Messaging!
- And it doesn't suck!
<small spiel about OwnPush>
- If this is of interest, talk to us - we need to work out what developers want and need!
- Might help reaching new markets without Google Services, or if you are just worried about Google banning your app
- Sometimes the best innovation disrupts the big players
- The world is becoming rather heavily centred on Google and Apple, and other "gatekeepers"
- Google was started by 2 guys in a garage, wanting to build something new and cool, and they disrupted tech big-time
- Today it's getting increasingly hard to disrupt the entrenched "big players" as they have power over you
- As developers, let's take back control. Let's innovate and disrupt, and make somewhere we can build disruptive technologies without being shut down by the big players
- And let's do it together!
XDA Labs - Community-driven distribution and discussion platform