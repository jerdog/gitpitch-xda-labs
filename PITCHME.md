#HSLIDE
![Image-Absolute](assets/xda-inverse-transparent-logo.png)

#HSLIDE

## XDA Labs
### Community-driven App Distribution
![Image-Absolute](assets/xdalabs_logo.png)

#HSLIDE
## Distribution Today

- Mostly Google Play <!-- .element: class="fragment" -->
- Pay 30% to play! <!-- .element: class="fragment" -->

#HSLIDE?image=http://www.reactiongifs.com/wp-content/uploads/2013/04/sad_face.gif

#HSLIDE
## The Problem?

- Being an app developer means thinking like a businessperson 
- Relying on one distribution channel is a big risk <!-- .element: class="fragment" -->
- Countless horror stories online about Google Play for devs <!-- .element: class="fragment" -->

#VSLIDE
### Google Play
- Google is a big company, and they aren't always developer-focused
- Issues are very often handled by automated email "bots" <!-- .element: class="fragment" -->
- Bot loops aren't unheard of - no way to escalate to a real human <!-- .element: class="fragment" -->

#VSLIDE
### Google Play (cont)
- Not brilliant when your app is offline, or under threat of removal! 
- Google likes to shoot first, and (maybe) question later <!-- .element: class="fragment" -->
- This might be good for users, but it's not good for developers! <!-- .element: class="fragment" -->

#VSLIDE
### The Solution?
- A platform for app distribution that's not hostile to developers 
- A platform where you can talk to a real person - you're not another number <!-- .element: class="fragment" -->
- But also a platform that's willing to innovate and make new things <!-- .element: class="fragment" -->

#VSLIDE
### Innovation?
- Gift a paid app to someone else?
- Alternative payment methods? <!-- .element: class="fragment" -->
- Try to improve the review/rating experience <!-- .element: class="fragment" -->

#VSLIDE
### Reviews and Ratings?
- "Makes my phone lag badly. [Samsung Galaxy Ace]" 
- "Doesn't work for me" <!-- .element: class="fragment" -->
- "Deceptive feature costs: Purchasing the application through in app menu only purchases one feature" <!-- .element: class="fragment" -->
- "1 star. Doesn't make my coffee" (OK, not literally) <!-- .element: class="fragment" -->

#VSLIDE
### Reviews and Ratings?
- This is pretty horrible for an independent developer
- Users blame app devs for bugs on their handsets (ahem, Samsung!) <!-- .element: class="fragment" -->
- Some people don't read the instructions for more technical apps, then complain they don't work! <!-- .element: class="fragment" -->

#VSLIDE
### Reviews and Ratings?
- Apps with IAPs explain what each one is for, but again people don't read... 
- And users expect apps to do things they don't even claim to do <!-- .element: class="fragment" -->
- Often things which Play won't let developers do... <!-- .element: class="fragment" -->

#VSLIDE
### Reviews and Ratings?
- Reviews often used for "ransom" 
- "Add X for 5 stars" <!-- .element: class="fragment" -->
- Reviews are short, developer responses even shorter <!-- .element: class="fragment" -->

#VSLIDE
### Reviews and Ratings?
- Maybe reviews need to "fade" over time, especially when referring to old versions 
- Some reviews are frankly more insightful than others <!-- .element: class="fragment" -->
  - We need to prioritise well-informed, helpful reviews (be they positive or negative)   <!-- .element: class="fragment" -->

#VSLIDE
### Lofty Goals?
- Yep!
- But the Play developer experience has been fairly stagnant for years <!-- .element: class="fragment" -->


#HSLIDE
## Presenting...

#VSLIDE
![Image-Absolute](assets/xdalabs_logo.png)

#VSLIDE
### XDA Labs
- By the team at XDA
- Building a new platform for distribution of apps <!-- .element: class="fragment" -->
- Community-first, by devs for the community <!-- .element: class="fragment" -->
- Devs deserve another choice <!-- .element: class="fragment" -->

#HSLIDE
## What is XDA Labs?
- The best way to browse the XDA forums on mobile - ad-free and fast
- Access news articles from the XDA Portal directly the app <!-- .element: class="fragment" -->
- The fastest way to get stable/beta/alpha builds of your favorite apps. <!-- .element: class="fragment" -->
- The most developer- and community-centric way to distribute your apps. <!-- .element: class="fragment" -->

#HSLIDE
## Accessing the XDA Forums
- Access the XDA Forum via our own API, designed for speed  
- Built entirely in-house giving us full control over every aspect of its feature set and functionality <!-- .element: class="fragment" -->

#VSLIDE
![Image-Absolute](assets/xda-forum-1.png)

#VSLIDE
![Image-Absolute](assets/xda-forum-2.png)

#VSLIDE
### For Your Device
- Better integration with the device you're accessing XDA on via My Device
- Automatically detects your device and displays the most popular threads and direct links to the full device forum<!-- .element: class="fragment" -->

#VSLIDE
![Image-Absolute](assets/xda-my-device.png)

#VSLIDE
### Future Plans

- Improving search
- Integrate community reviews
- Ability to browse and install ROMs


#HSLIDE
## Apps / Xposed Mods / Wallpapers

#VSLIDE
### Stats:
- 400 apps  <!-- .element: class="fragment" -->
- 1000+ Xposed modules <!-- .element: class="fragment" -->
- 6500+ Labs Developer accounts <!-- .element: class="fragment" -->
- 225k+ Labs users <!-- .element: class="fragment" -->
  - Averaging 1500+ new users per day <!-- .element: class="fragment" -->
  - 1 million sessions per month <!-- .element: class="fragment" -->

#VSLIDE
### More Stats:
- Translated into 30 languages <!-- .element: class="fragment" -->
- Active users doubled in the past 2 months <!-- .element: class="fragment" -->
- Active users loading the app 4-5 times per day <!-- .element: class="fragment" -->
- User makeup is truly international <!-- .element: class="fragment" -->
  - 30% Asia <!-- .element: class="fragment" -->
  - 28% North America <!-- .element: class="fragment" -->
  - 27% Europe <!-- .element: class="fragment" -->
  - 15% Mars <!-- .element: class="fragment" -->

#VSLIDE
### Key Features
- No ads 
- App distribution platform for hobbiest and professional developers <!-- .element: class="fragment" -->
- Not a replacement for Play - can co-exist <!-- .element: class="fragment" -->
- Support for Alpha, Beta, and Stable release channels <!-- .element: class="fragment" -->

#VSLIDE
![Image-Absolute](assets/xda-labs-app-listings.png)

#VSLIDE
### Key Features
- Built-in forum integration on XDA, allowing for interactive communication with users
- Built-in commerce for devs where developers get 100% of app revenue (Play gives 70% before taxes) <!-- .element: class="fragment" -->
  - Accept purchases via Paypal or Bitcoin <!-- .element: class="fragment" -->

#VSLIDE
![Image-Absolute](assets/xda-labs-app-forums.png)

#VSLIDE
### Xposed Integration
- Xposed is an innovative framework for rooted devices, allowing users to modify the system without a custom ROM 
- All Xposed modules are at your fingertips  <!-- .element: class="fragment" -->

#VSLIDE
![Image-Absolute](assets/xda-labs-xposed.png)

#VSLIDE
### Other Goodies
- Wallpaper download and install

#VSLIDE
![Image-Absolute](assets/xda-labs-wallpaper.png)

#VSLIDE
### Other Goodies
- Wallpaper download and install 
- Receive notification of updates to any app on your device, if it's in Labs, regardless of distribution platform used (Play, Amazon, etc.) 


#HSLIDE
## Why Labs?
- Bootstrapping on XDA's established reputation in the community
- More technical than average audience <!-- .element: class="fragment" -->
- Good proving ground for your app <!-- .element: class="fragment" -->
- Don't worry about Google killing your developer account! <!-- .element: class="fragment" -->
- Innovate freely, and build cool things <!-- .element: class="fragment" -->

#VSLIDE
### Play Restrictions?
- No ad blockers, no network blocking, etc etc.
- Devs hate ad blockers, but even Apple is going in the direction of allowing it! <!-- .element: class="fragment" -->
- Semi-arbitrary rules about keywords, and seemingly semi-arbitrary enforcement <!-- .element: class="fragment" -->
- Sometimes Google remove apps they find questionable (NBZ360, Usenet reader) <!-- .element: class="fragment" -->
  - Don't ban the technology just because people can use it for bad things! <!-- .element: class="fragment" -->

#VSLIDE
![Image-Absolute](assets/app-suspended.jpg)

#VSLIDE
### What if they decide you break a rule?
- Vague, half-baked email that doesn't explain why 
- Often cannot rectify the issue as the app is "gone" <!-- .element: class="fragment" -->
- User-base lost, customers dissatistifed <!-- .element: class="fragment" -->
- Unclear if new version will be accepted <!-- .element: class="fragment" -->
- Canned responses without information <!-- .element: class="fragment" -->

#VSLIDE
### Pro-Developer
- We allow all types of apps (including stuff not allowed on Play) 
- We don't remove apps without warning and without communication to the developer <!-- .element: class="fragment" -->
- We enable developers to sell apps, receiving 100% of the proceeds directly <!-- .element: class="fragment" -->
- If you have an issue, there's a real person to work with, not an algorhithm or robot <!-- .element: class="fragment" -->

#HSLIDE
## Easy to Add
- Not complicated
- Quick submission process <!-- .element: class="fragment" -->
- No need to follow 10 steps to make sure you don't get kicked out <!-- .element: class="fragment" -->

#VSLIDE
![Image-Absolute](assets/10-tips-google-play.png)

#VSLIDE
### Easy to Add
- Not complicated
- Quick submission process
- No need to follow 10 steps to make sure you don't get kicked out
- Only 3 steps required (4 if your app is for sale) <!-- .element: class="fragment" -->

#VSLIDE
### Steps to Add
- Login to https://labs.xda-developers.com/ with your XDA account

#VSLIDE
![Image-Absolute](assets/xda-labs-cache.png)

#VSLIDE
### Steps to Add
- Login to https://labs.xda-developers.com/ with your XDA account
- Add your payment info to your settings 

#VSLIDE
![Image-Absolute](assets/xda-labs-settings.png)

#VSLIDE
### Steps to Add
- Login to https://labs.xda-developers.com/ with your XDA account
- Add your payment info to your settings
- Fill out info for new app 

#VSLIDE
![Image-Absolute](assets/xda-labs-add-app.png)

#VSLIDE
### Steps to Add
- Login to https://labs.xda-developers.com/ with your XDA account
- Add your payment info to your settings
- Fill out info for new app
- Add screenshot(s) 

#VSLIDE
![Image-Absolute](assets/xda-labs-add-screenshots.png)

#VSLIDE
### Steps to Add
- Login to https://labs.xda-developers.com/ with your XDA account
- Add your payment info to your settings
- Fill out info for new app
- Add screenshot(s)
- Publish 

#VSLIDE?image=assets/773.gif


#HSLIDE
## Get XDA Labs
![Image-Absolute](assets/xda_labs_qr_big-300x300.png)


#HSLIDE
## Questions
