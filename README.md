# XDA Labs
## Presentation using GitPitch
Link: https://gitpitch.com/jerdog/gitpitch-xda-labs/master?grs=gitlab

## Offline Presenting
- Download: https://gitpitch.com/pitchme/offline/gitlab/jerdog/gitpitch-xda-labs/master/white/PITCHME.zip
- Unzip to local directory
- Run the following (found in local_view.sh)

`python -m SimpleHTTPServer`

- Load up a webbrowser and link to http://localhost:8000
